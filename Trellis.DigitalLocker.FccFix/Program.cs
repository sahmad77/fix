using Amazon.SQS;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NLog;
using NLog.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using QuickFix;
using Trellis.DigitalLocker.FccFix.Models;
using Trellis.DigitalLocker.FccFix.IServices;
using Trellis.DigitalLocker.FccFix.Services;

namespace Trellis.DigitalLocker.FccFix
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var builder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json")
                        .AddEnvironmentVariables()
                        .Build();
                })
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddDefaultAWSOptions(hostContext.Configuration.GetAWSOptions());
                    services.AddAWSService<IAmazonSQS>();
                    services.AddLogging(logging =>
                    {
                        var config = new NLog.Config.LoggingConfiguration();
                        var logconsole = new NLog.Targets.ConsoleTarget("logconsole");
                        config.AddRule(GetLogLevel(), LogLevel.Fatal, logconsole);
                        logging.AddNLog(config);
                    });
                    services.AddOptions();
                    services.Configure<ConfigOptions>(hostContext.Configuration);
                    services.AddTransient<IFixClient, FixClientService>();
                    services.AddTransient<IQueueService, SQSManager>();
                    services.AddTransient<ISSMService, SSMService>();

                    services.AddHostedService<Worker>();
                });

        private static LogLevel GetLogLevel()
        {
            var log = LogLevel.Info;
            string logLevel = Environment.GetEnvironmentVariable("LOGLEVEL");

            switch (logLevel)
            {
                case "Trace":
                    log = LogLevel.Trace;
                    break;
                case "Debug":
                    log = LogLevel.Debug;
                    break;
                case "Info":
                    log = LogLevel.Info;
                    break;
                case "Warn":
                    log = LogLevel.Warn;
                    break;
                case "Error":
                    log = LogLevel.Error;
                    break;
                case "Fatal":
                    log = LogLevel.Fatal;
                    break;
                case "Off":
                    log = LogLevel.Off;
                    break;

                default:
                    break;
            }

            return log;
        }
    }
}
