﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trellis.DigitalLocker.FccFix.Models
{
    public class FixBatchOrder
    {
        public string OrderId { get; set; }

        public List<FixOrders> FixOrders { get; set; }

        public FixBatchOrder()
        {
            FixOrders = new List<FixOrders>();
        }
    }
    public class FixAccount
    {
        public string AccountNumber { get; set; }

        public decimal AllocShares { get; set; }
    }
    public class FixOrders
    {
        public FixOrders()
        {
            Accounts = new List<FixAccount>();
        }

        public string OrderType { get; set; }
        public string OrderId { get; set; }

        public string Side { get; set; }

        public string Symbol { get; set; }

        public string Exchange { get; set; }

        public decimal Qty { get; set; }

        public decimal Price { get; set; }

        public string Currency { get; set; }

        public string Country { get; set; }

        public List<FixAccount> Accounts { get; set; }
    }
}
