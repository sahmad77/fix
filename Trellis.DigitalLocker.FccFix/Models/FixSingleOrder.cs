﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trellis.DigitalLocker.FccFix.Models
{
    public class FixSingleOrder
    {
        public string OrderId { get; set; }

        public string Side { get; set; }

        public string Symbol { get; set; }

        public string Exchange { get; set; }

        public decimal Qty { get; set; }

        public decimal Price { get; set; }

        public string Currency { get; set; }

        public string Country { get; set; }

        public string Account { get; set; }
        public string OrderType { get; set; }

        public FixSingleOrder()
        {
        }

    }
}
