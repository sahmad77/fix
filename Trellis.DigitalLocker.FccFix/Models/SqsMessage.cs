﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trellis.DigitalLocker.FccFix.Models
{
    public class SqsMessage
    {
        public string MessageId { get; set; }

        public string MessageBody { get; set; }

        public string AccountId { get; set; }

        public FixMessageType MessageType { get; set; }
    }

    public enum FixMessageType
    {
        SdaOrder,
        RoboOrder
    }
}
