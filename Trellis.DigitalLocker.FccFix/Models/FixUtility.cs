﻿using QuickFix.Fields;
using System;
using System.Collections.Generic;
using System.Text;

namespace Trellis.DigitalLocker.FccFix.Models
{
    public class FixUtility
    {
        public static string GetStatusMessageByExecType(char execType)
        {
            switch (execType)
            {
                case ExecType.PENDING_NEW:
                    return "pending";

                case ExecType.CANCELED:
                    return "canceled";

                case ExecType.REPLACE:
                    return "replace";

                case ExecType.EXPIRED:
                    return "expired";

                case ExecType.REJECTED:
                    return "rejected";

                case ExecType.FILL:
                    return "filled";

                case ExecType.PARTIAL_FILL:
                    return "partial_fill";

                case ExecType.NEW:
                    return "new";

                default:
                    return string.Empty;
            }
        }
    }
}
