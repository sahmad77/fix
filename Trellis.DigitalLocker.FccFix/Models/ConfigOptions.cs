﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trellis.DigitalLocker.FccFix.Models
{
    public class ConfigOptions
    {
        public string ReaderQueueServiceUrl { get; set; }

        public int ReaderWaitTimeInSeconds { get; set; }

        public string WriteQueueServiceUrl { get; set; }
    }
}
