﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Trellis.DigitalLocker.FccFix.Models
{
    public class FixExecutionReport
    {
        public string OrderId { get; set; }

        public string Status { get; set; }

        public string AccountId { get; set; }

        public decimal LeavesQty { get; set; }

        public decimal CumQty { get; set; }

        public string OrderStatus { get; set; }
        public string RejectReason { get; set; }
    }
}
