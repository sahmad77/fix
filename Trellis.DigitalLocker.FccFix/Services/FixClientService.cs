﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using QuickFix;
using QuickFix.Fields;
using System;
using System.Collections.Generic;
using System.Text;
using Trellis.DigitalLocker.FccFix.IServices;
using Trellis.DigitalLocker.FccFix.Models;

namespace Trellis.DigitalLocker.FccFix.Services
{
    public class FixClientService : QuickFix.MessageCracker, IFixClient
    {
        private readonly ILogger<FixClientService> _logger;
        private readonly IQueueService _queueService;
        Session _session = null;

        public FixClientService(ILogger<FixClientService> logger, IQueueService queueService)
        {
            _logger = logger;
            _queueService = queueService;
            
        }

        public void SingleOrder(FixSingleOrder model)
        {
            QuickFix.FIX42.NewOrderSingle order = new QuickFix.FIX42.NewOrderSingle(
               new ClOrdID(model.OrderId.ToString()), // Confirm about order id mapped to id ?
               new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PUBLIC),
               new Symbol(model.Symbol), //model.Symbol not mapped
               new Side(model.Side.ToLower() == "buy" ? Side.BUY : Side.SELL),
               new TransactTime(DateTime.UtcNow),
               new OrdType(model.Side.ToLower() == "buy" || model.Side.ToLower() == "limit" ? OrdType.LIMIT : OrdType.MARKET)
               );

            order.SetField(new Account(model.Account));
            order.SetField(new OrderQty(model.Qty));
            if (model.Side.ToLower() == "buy" || model.OrderType.ToLower() == "limit")
            {
                order.SetField(new TimeInForce(TimeInForce.GOOD_TILL_DATE));
                order.SetField(new ExpireDate(DateTime.UtcNow.ToString("yyyyMMdd")));
                order.Set(new Price(model.Price));
            }
            order.Header.GetString(Tags.BeginString);
            order.Header.GetString(Tags.BodyLength);
            order.Header.SetField(new MsgType("D"));
            //order.Header.GetString(Tags.MsgType);
            order.Header.SetField(new SenderCompID(""));
            order.Header.SetField(new TargetCompID(""));
            order.Header.GetString(Tags.MsgSeqNum);
            order.Header.SetField(new SendingTime(DateTime.UtcNow));

            order.Trailer.GetString(Tags.CheckSum);

            SendMessage(order);
        }

        public void RoboOrder(FixBatchOrder model)
        {
            foreach (var item in model.FixOrders)
            {
                QuickFix.FIX42.NewOrderSingle order = new QuickFix.FIX42.NewOrderSingle();

                order.SetField(new ClOrdID(item.OrderId));
                order.SetField(new HandlInst(HandlInst.AUTOMATED_EXECUTION_ORDER_PUBLIC));
                order.SetField(new Symbol(item.Symbol));
                order.SetField(new Side(item.Side.ToLower() == "buy" ? Side.BUY : Side.SELL));
                order.SetField(new TransactTime(DateTime.UtcNow));
                order.SetField(new OrdType(item.Side.ToLower() == "buy" || item.OrderType == "limit" ? OrdType.LIMIT : OrdType.MARKET));
                order.SetField(new OrderQty(item.Qty));
                order.SetField(new TimeInForce(TimeInForce.DAY));

                if (item.Side.ToLower() == "buy" || item.OrderType.ToLower() == "limit")
                    order.Set(new Price(item.Price));

                // if (!string.IsNullOrEmpty(item.Exchange))
                //     order.SetField(new SecurityExchange(item.Exchange));

                if (!string.IsNullOrEmpty(item.Country))
                    order.SetField(new Country(item.Country));

                if (!string.IsNullOrEmpty(item.Currency))
                    order.SetField(new Currency(item.Currency));

                order.NoAllocs = new NoAllocs(item.Accounts.Count);

                foreach (var account in item.Accounts)
                {
                    QuickFix.FIX42.Allocation.NoAllocsGroup group = new QuickFix.FIX42.Allocation.NoAllocsGroup();
                    group.AllocAccount = new AllocAccount(account.AccountNumber);
                    group.AllocShares = new AllocShares(account.AllocShares);

                    order.AddGroup(group);
                }

                order.Header.GetString(Tags.BeginString);
                order.Header.GetString(Tags.BodyLength);
                order.Header.SetField(new MsgType("D"));
                //order.Header.GetString(Tags.MsgType);
                order.Header.SetField(new SenderCompID(""));
                order.Header.SetField(new TargetCompID(""));
                order.Header.GetString(Tags.MsgSeqNum);
                order.Header.SetField(new SendingTime(DateTime.UtcNow));

                order.Trailer.GetString(Tags.CheckSum);

                SendMessage(order);
            }
        }
        public void CanceleOrder(FixSingleOrder model)
        {
            throw new NotImplementedException();
        }

        public void OrderStatus(string clOrderId)
        {
            QuickFix.FIX42.OrderStatusRequest r = new QuickFix.FIX42.OrderStatusRequest();
            r.ClOrdID = new ClOrdID(clOrderId);

            r.Header.GetString(Tags.BeginString);
            SendMessage(r);
        }


        #region IApplication interface overrides
        public void OnCreate(SessionID sessionID)
        {
            _session = Session.LookupSession(sessionID);
        }

        

        public void FromApp(Message message, SessionID sessionID)
        {
            try
            {
                Crack(message, sessionID);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                _logger.LogError(ex.StackTrace);
            }
        }
        public void ToApp(Message message, SessionID sessionID)
        {
            try
            {
                bool possDupFlag = false;
                if (message.Header.IsSetField(QuickFix.Fields.Tags.PossDupFlag))
                {
                    possDupFlag = QuickFix.Fields.Converters.BoolConverter.Convert(
                        message.Header.GetString(QuickFix.Fields.Tags.PossDupFlag)); /// FIXME
                }
                if (possDupFlag)
                    throw new DoNotSend();
            }
            catch (FieldNotFoundException ex)
            {
                _logger.LogError(ex.ToString());
            }

            Console.WriteLine();
        }


        public void OnLogon(SessionID sessionID){Console.WriteLine("Logon - " + sessionID.ToString());}

        public void OnLogout(SessionID sessionID){Console.WriteLine("Logout - " + sessionID.ToString());}

        public void FromAdmin(Message message, SessionID sessionID){}
        public void ToAdmin(Message message, SessionID sessionID){}

        #endregion

        #region MessageCracker handlers
        public void OnMessage(QuickFix.FIX42.ExecutionReport m, SessionID s)
        {
            try
            {
                if (m is QuickFix.FIX42.ExecutionReport)
                {
                    QuickFix.FIX42.ExecutionReport er = (QuickFix.FIX42.ExecutionReport)m;

                    var model = new FixExecutionReport()
                    {
                        //AccountId = er.Account.getValue(),
                        OrderId = er.ClOrdID.getValue(),
                        CumQty = er.CumQty.getValue(),
                        LeavesQty = er.LeavesQty.getValue(),
                        OrderStatus = JsonConvert.SerializeObject(er)
                    };

                    model.Status = FixUtility.GetStatusMessageByExecType(er.ExecType.getValue());

                    try
                    {
                        //var workItem = new WorkItem
                        //{
                        //    Id = Guid.NewGuid(),
                        //    Type = "fcc.order_status.update",
                        //    Data = JsonConvert.SerializeObject(model),
                        //    Attempts = 0,
                        //    NextAttempt = DateTime.UtcNow.AddMilliseconds(-1),
                        //    Status = Constants.WorkItemStatusValue.Submitted,
                        //    LastResult = string.Empty,
                        //    OrganizationId = new Guid("C8D6F07B-ACE4-4D64-A660-630B1E4D4BA2"),
                        //    CreatedAt = DateTime.UtcNow
                        //};

                        //_dbContext.WorkItems.Add(workItem);
                        //_dbContext.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        _logger.LogError(ex.ToString());
                    }

                    //string report = JsonConvert.SerializeObject(model);
                    //_queueService.SendMessageAsync(report);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                _logger.LogError(ex.ToString());
            }
        }
        public void OnMessage(QuickFix.FIX42.OrderCancelReject m, SessionID s)
        {
            Console.WriteLine("Received order cancel reject");
        }
        
        #endregion


        private void SendMessage(Message m)
        {
            if (_session != null)
                _session.Send(m);
            else
            {
                // This probably won't ever happen.
                _logger.LogError("Can't send message: session not created.");
            }
        }

        
    }
}
