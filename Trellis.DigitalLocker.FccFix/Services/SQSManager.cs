﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Trellis.DigitalLocker.FccFix.IServices;
using Trellis.DigitalLocker.FccFix.Models;

namespace Trellis.DigitalLocker.FccFix.Services
{
    public class SQSManager : IQueueService
    {
        private readonly ILogger<SQSManager> _logger;

        private readonly IAmazonSQS _sqs;

        private readonly ISSMService _ssmService;

        private readonly ConfigOptions _options;

        public SQSManager(ILogger<SQSManager> logger, IOptions<ConfigOptions> options, IAmazonSQS sqs, ISSMService ssmService)
        {
            _sqs = sqs;
            _logger = logger;
            _options = options.Value;
            _ssmService = ssmService;
        }

        public async Task<List<SqsMessage>> ReceiveMessageAsync()
        {
            List<SqsMessage> sqlMessage = new List<SqsMessage>();
            try
            {
                // "sqs/fcc-fix-request-queue-url"
                var queueUrl = await _ssmService.GetValueAsync("sqs/fcc-fix-request-queue-url");

                var request = new ReceiveMessageRequest()
                {
                    QueueUrl = queueUrl,
                    WaitTimeSeconds = 5 // Convert.ToInt32(Environment.GetEnvironmentVariable("ReaderWaitTimeInSeconds"))
                };

                var result = await _sqs.ReceiveMessageAsync(request);

                if (result.Messages.Any())
                {
                    _logger.LogInformation($"Processing messages");

                    foreach (var message in result.Messages)
                    {
                        sqlMessage.Add(JsonConvert.DeserializeObject<SqsMessage>(message.Body));
                        await DeleteMessageAsync(request.QueueUrl, message.ReceiptHandle);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException.ToString());
            }

            return sqlMessage;
        }

        public async Task<bool> SendMessageAsync(string message)
        {
            try
            {
                var queueUrl = await _ssmService.GetValueAsync("sqs/fcc-fix-request-queue-url");
                var request = new SendMessageRequest()
                {
                    QueueUrl = queueUrl,
                    MessageBody = message
                };

                var result = await _sqs.SendMessageAsync(request);

                if (result.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    _logger.LogInformation($"Processing message");
                    return true;
                }

                _logger.LogError(result.ResponseMetadata.ToString());
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException.ToString());
                return false;
            }
        }
        public async Task<bool> DeleteMessageAsync(string url, string receiptHandle)
        {
            try
            {
                var request = new DeleteMessageRequest()
                {
                    QueueUrl = url,
                    ReceiptHandle = receiptHandle,
                };

                var result = await _sqs.DeleteMessageAsync(request);

                if (result.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    _logger.LogInformation($"Deleting message");
                    return true;
                }

                _logger.LogError($"Deleting message {result.HttpStatusCode} - {result.ResponseMetadata}");
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException.ToString());
                return false;
            }
        }
        public async Task<bool> DeleteMessageAsync(string receiptHandle)
        {
            try
            {
                var queueUrl = await _ssmService.GetValueAsync("sqs/fcc-fix-request-queue-url");

                var request = new DeleteMessageRequest()
                {
                    QueueUrl = queueUrl,
                    ReceiptHandle = receiptHandle,
                };

                var result = await _sqs.DeleteMessageAsync(request);

                if (result.HttpStatusCode == System.Net.HttpStatusCode.OK)
                {
                    _logger.LogInformation($"Deleting message");
                    return true;
                }

                _logger.LogError($"Deleting message {result.HttpStatusCode} - {result.ResponseMetadata}");
                return false;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.InnerException.ToString());
                return false;
            }
        }
    }
}
