﻿using Amazon;
using Amazon.SimpleSystemsManagement;
using Amazon.SimpleSystemsManagement.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trellis.DigitalLocker.FccFix.IServices;

namespace Trellis.DigitalLocker.FccFix.Services
{
    public class SSMService : ISSMService
    {
        private string _environment;
        private readonly AmazonSimpleSystemsManagementClient client;


        public SSMService()
        {
            //_environment =  EnvironmentExtension.GetEnvironmentName();
            _environment = Environment.GetEnvironmentVariable("");
            client = new AmazonSimpleSystemsManagementClient(RegionEndpoint.USEast1);
        }
        public async Task<string> GetValueAsync(string key)
        {
            var getParameterResponse = await client.GetParameterAsync(new GetParameterRequest() { Name = $"/{_environment}/{key}", WithDecryption = true });
            return getParameterResponse.Parameter.Value;
        }
    }
}
