using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Trellis.DigitalLocker.FccFix.IServices;
using Trellis.DigitalLocker.FccFix.Models;

namespace Trellis.DigitalLocker.FccFix
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        
        private readonly IFixClient _fixClient;
        private readonly IQueueService _queueService;
        //private readonly string file = EnvironmentExtension.IsProduction() ? @"tradeclientprod.cfg" : @"tradeclient.cfg";
        private readonly string file = Environment.GetEnvironmentVariable("") == "production" ? @"tradeclientprod.cfg" : @"tradeclient.cfg";
        private readonly QuickFix.Transport.SocketInitiator initiator;

        public Worker(ILogger<Worker> logger, IFixClient fixClient, IQueueService queue)
        {
            _logger = logger;
            _fixClient = fixClient;
            _queueService = queue;
            QuickFix.SessionSettings settings = new QuickFix.SessionSettings(file);
            QuickFix.IMessageStoreFactory storeFactory = new QuickFix.FileStoreFactory(settings);
            QuickFix.ILogFactory logFactory = new QuickFix.ScreenLogFactory(settings);
            initiator = new QuickFix.Transport.SocketInitiator(_fixClient, storeFactory, settings, logFactory);

            try
            {
                var req = (HttpWebRequest)WebRequest.Create("https://api.ipify.org/");
                var response = (HttpWebResponse)req.GetResponse();
                using var sr = new StreamReader(response.GetResponseStream());
                var result = sr.ReadToEnd();
                _logger.LogInformation($"\n External Request IP: \n{result}");

            }
            catch (Exception ex)
            {
                _logger.LogInformation($"\n External request exception: \n{ex.Message}");
            }
            initiator.Start();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                if (initiator.IsLoggedOn && !initiator.IsStopped)
                {
                    try
                    {
                        var sqsMessages = await _queueService.ReceiveMessageAsync();
                        foreach (var message in sqsMessages)
                        {
                            try
                            {
                                switch (message.MessageType)
                                {
                                    case Models.FixMessageType.SdaOrder:
                                        var sdaOrder = JsonConvert.DeserializeObject<FixSingleOrder>(message.MessageBody);
                                        //_logger.LogInformation($"Processing on FIX at: {DateTimeOffset.UtcNow} - Account ID: {sdaOrder.Account}, Order ID: {sdaOrder.OrderId}");
                                        _fixClient.SingleOrder(sdaOrder);
                                        break;
                                    case Models.FixMessageType.RoboOrder:
                                        var roboOrder = JsonConvert.DeserializeObject<FixBatchOrder>(message.MessageBody);
                                        //_logger.LogInformation($"Processing on FIX at: {DateTimeOffset.UtcNow} - Batch ID: {roboOrder.OrderId}");
                                        _fixClient.RoboOrder(roboOrder);
                                        break;
                                    default:
                                        break;
                                }

                            }
                            catch (Exception ex)
                            {

                                _logger.LogError($"{ex.Message} - {ex.StackTrace}"); ;
                            }
                        }
                    }
                    catch (Exception e)
                    {

                        _logger.LogError($"{e.Message} - {e.StackTrace}");
                    }
                }
                _logger.LogWarning($"FIX client is closed.");
                initiator.Stop();
                //_logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now);
                //await Task.Delay(1000, stoppingToken);
            }
        }
    }
}
