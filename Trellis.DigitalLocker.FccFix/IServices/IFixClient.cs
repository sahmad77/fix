﻿using QuickFix;
using System;
using System.Collections.Generic;
using System.Text;
using Trellis.DigitalLocker.FccFix.Models;

namespace Trellis.DigitalLocker.FccFix.IServices
{
    public interface IFixClient : IApplication
    {
        void OrderStatus(string clOrderId);

        void SingleOrder(FixSingleOrder model);

        void CanceleOrder(FixSingleOrder model);

        void RoboOrder(FixBatchOrder model);

    }
}
