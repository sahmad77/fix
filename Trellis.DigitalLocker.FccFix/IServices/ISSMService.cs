﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Trellis.DigitalLocker.FccFix.IServices
{
    public interface ISSMService
    {
        Task<string> GetValueAsync(string key);
    }
}
