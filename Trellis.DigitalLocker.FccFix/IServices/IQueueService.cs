﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Trellis.DigitalLocker.FccFix.Models;

namespace Trellis.DigitalLocker.FccFix.IServices
{
    public interface IQueueService
    {
        Task<List<SqsMessage>> ReceiveMessageAsync();

        Task<bool> SendMessageAsync(string message);

        Task<bool> DeleteMessageAsync(string receiptHandle);

        Task<bool> DeleteMessageAsync(string url, string receiptHandle);
    }
}
